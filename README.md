# JIRA Velocity Html Escaping Examples

Contains examples that demonstrate the automatic html encoding features provided by the velocity engine in JIRA.

An accompanying guide is available at https://developer.atlassian.com/display/JIRADEV/Velocity+Templates
